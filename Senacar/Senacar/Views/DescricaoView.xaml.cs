﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Senacar.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class DescricaoView : ContentPage
	{
        private const int VIDROS_ELETRICOS =  545;
        private const int TRAVAS_ELETRICAS =  260;
        private const int AR_CONDICIONADO =  480;
        private const int CAMERA_DE_RE =  180;
        private const int CAMBIO =  460;
        private const int SUSPENSAO = 380;
        private const int FREIOS =  245;

        public string ValorTotal
        {
            get
            {
                return string.Format("Valor Total: R$ {0}", Veiculos.Preco
                    + (IncluiVidrosEletricos ? VIDROS_ELETRICOS : 0)
                    + (IncluiTravasEletricas ? TRAVAS_ELETRICAS : 0)
                    + (IncluiArCondicionado ? AR_CONDICIONADO : 0)
                    + (IncluiCameraDeRe ? CAMERA_DE_RE : 0)
                    + (IncluiCambio ? CAMBIO : 0)
                    + (IncluiSuspensao ? SUSPENSAO : 0)
                    + (IncluiFreios ? FREIOS : 0));
            }
        }

        bool incluiVidrosEletricos;

        public bool IncluiVidrosEletricos
        {
            get
            {
                return incluiVidrosEletricos;
            }
            set
            {
                incluiVidrosEletricos = value;
               
                OnPropertyChanged(nameof(ValorTotal));
            }
        }

        bool incluiTravasEletricas;

        public bool IncluiTravasEletricas
        {
            get
            {
                return incluiTravasEletricas;
            }
            set
            {
                incluiTravasEletricas = value;
              
                OnPropertyChanged(nameof(ValorTotal));
            }
        }

        bool incluiArCondicionado;

        public bool IncluiArCondicionado
        {
            get
            {
                return incluiArCondicionado;
            }
            set
            {
                incluiArCondicionado = value;
             
                OnPropertyChanged(nameof(ValorTotal));
            }
        }
        bool incluiCameraDeRe;

        public bool IncluiCameraDeRe
        {
            get
            {
                return incluiCameraDeRe;
            }
            set
            {
                incluiCameraDeRe = value;
                
                OnPropertyChanged(nameof(ValorTotal));
            }
        }

        bool incluiCambio;

        public bool IncluiCambio
        {
            get
            {
                return incluiCambio;
            }
            set
            {
                incluiCambio = value;
              
                OnPropertyChanged(nameof(ValorTotal));
            }
        }

        bool incluiSuspensao;

        public bool IncluiSuspensao
        {
            get
            {
                return incluiSuspensao;
            }
            set
            {
                incluiSuspensao = value;
               
                OnPropertyChanged(nameof(ValorTotal));
            }
        }

        bool incluiFreios;

        public bool IncluiFreios
        {
            get
            {
                return incluiFreios;
            }
            set
            {
                incluiFreios = value;
                
                OnPropertyChanged(nameof(ValorTotal));
            }
        }

       

        public string TextoVidrosEletricos
        {
            get
            {
                return string.Format("Vidros Elétricos- R$ {0}", VIDROS_ELETRICOS);
            }
        }
        public string TextoTravasEletricas
        {
            get
            {
                return string.Format("Travas Elétricas- R$ {0}", TRAVAS_ELETRICAS);
            }
        }
        public string TextoArCondicionado
        {
            get
            {
                return string.Format("Ar Condicionado- R$ {0}", AR_CONDICIONADO);
            }
        }
        public string TextoCameraDeRe
        {
            get
            {
                return string.Format("Camera de ré- R$ {0}", CAMERA_DE_RE);
            }
        }
        public string TextoCambio
        {
            get
            {
                return string.Format("Cambio- R$ {0}", CAMBIO);
            }
        }
        public string TextoSuspensao
        {
            get
            {
                return string.Format("Suspensao- R$ {0}", SUSPENSAO);
            }
        }
        public string TextoFreios
        {
            get
            {
                return string.Format("Freios- R$ {0}", FREIOS);
            }
        }
        public Veiculos Veiculos{ get; set; }
		public DescricaoView (Veiculos veiculos)
		{
			InitializeComponent ();

            this.Title = veiculos.NomeMarca;
            this.Title = veiculos.NomeModelos;
            this.Veiculos = veiculos;
            this.BindingContext = this;
		}

        private void ButtonProximo_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new AgendamentoView(this.Veiculos));
        }
    }
}