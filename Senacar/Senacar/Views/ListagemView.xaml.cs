﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Senacar.Views
{

    public class Veiculos
    {
        public string NomeMarca { get; set; }

       
    
    
    
        public string NomeModelos { get; set; }
        public float Preco { get; set; }
        public string PrecoFormatado
        {
            get { return string.Format("R$ {0}", Preco); }
        }
    }

    
    public partial class ListagemView : ContentPage
    {
        public List<Veiculos> Veiculos { get; set; }
        




        public ListagemView()
        {
            InitializeComponent();

            

            this.Veiculos = new List<Veiculos>
            {
                new Veiculos { NomeMarca = "Chevrolet", NomeModelos = "Onix", Preco = 44000},
                new Veiculos { NomeMarca = "Hyundai", NomeModelos = "Hb20",Preco = 42000},
                new Veiculos { NomeMarca = "Renault", NomeModelos = "Sandero" ,Preco = 39000},
                new Veiculos { NomeMarca = "Ford", NomeModelos = "Fiesta", Preco = 29000},
                new Veiculos { NomeMarca = "Honda",NomeModelos = "Civic", Preco = 84000}
            };

            this.BindingContext = this;

           
            
               
            
        }

        private void ListViewSenacar_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            var veiculos = (Veiculos)e.Item;

            Navigation.PushAsync(new DescricaoView(veiculos));

            //DisplayAlert("Veiculos", string.Format("Voce selecionou o veiculo '{0}' . Modelo{1} . Preco{2}", veiculos.NomeMarca, veiculos.NomeModelos, veiculos.PrecoFormatado), "OK");
        }

       
    }
}
